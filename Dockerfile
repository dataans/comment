FROM eu.gcr.io/dataans/comment_builder:6 as builder

COPY ./Cargo.toml ./Cargo.toml
RUN rm src/*.rs

ADD src ./src
ADD deploy ./deploy

RUN rm ./target/release/deps/comment*
RUN cargo build --release

FROM frolvlad/alpine-glibc:glibc-2.29
ARG APP=/usr/src/app

EXPOSE 8000

COPY --from=builder /comment/target/release/comment ./comment

CMD ["./comment"]
