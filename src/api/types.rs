use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::Comment;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub enum CommentTypeRequest {
    Question(Uuid),
    Answer(Uuid),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct CommentCreate {
    pub text: String,
    pub sample_id: Option<Uuid>,
    pub comment_type: CommentTypeRequest,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CommentUpdate {
    pub id: Uuid,
    pub text: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct CommentResponse {
    pub id: Uuid,
    pub text: String,
    #[serde(with = "rfc3339")]
    pub created_at: OffsetDateTime,
    #[serde(with = "rfc3339")]
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub sample_id: Option<Uuid>,
    pub up_votes: i64,

    pub is_voted: bool,
}

impl CommentResponse {
    pub fn set_up_votes(&mut self, up_votes: i64) {
        self.up_votes = up_votes;
    }

    pub fn set_is_voted(&mut self, is_voted: bool) {
        self.is_voted = is_voted;
    }

    pub fn id(&self) -> &Uuid {
        &self.id
    }
}

impl From<Comment> for CommentResponse {
    fn from(comment: Comment) -> Self {
        let Comment {
            id,
            text,
            created_at,
            creator_id,
            updated_at,
            sample_id,
            ..
        } = comment;
        Self {
            id,
            text,
            created_at,
            creator_id,
            updated_at,
            sample_id,
            up_votes: 0,

            is_voted: false,
        }
    }
}

impl Responder for CommentResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

pub struct CommentsResponse(pub Vec<CommentResponse>);

impl Responder for CommentsResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self.0).unwrap())
    }
}

#[cfg(test)]
mod tests {
    use uuid::Uuid;

    use super::{CommentCreate, CommentTypeRequest};

    #[test]
    fn test_serialize() {
        let data = CommentCreate {
            text: "some_text".into(),
            sample_id: Some(Uuid::new_v4()),
            comment_type: CommentTypeRequest::Question(Uuid::new_v4()),
        };
        println!("{}", serde_json::to_string(&data).unwrap());
    }
}
