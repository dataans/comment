use actix_web::{
    delete, get, http::StatusCode, post, put, web, HttpRequest, HttpResponse, Responder,
};
use serde::Deserialize;
use uuid::Uuid;

use super::types::CommentCreate;
use crate::{api::types::CommentUpdate, app::AppData, services::comment::CommentError};

const AUTH_COOKIE_NAME: &str = "SessionId";

#[derive(Deserialize)]
pub struct Id {
    pub id: Uuid,
}

pub async fn default_route() -> impl Responder {
    HttpResponse::Ok().status(StatusCode::NOT_FOUND).body(())
}

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("comment ok.")
}

#[post("")]
pub async fn create_comment(
    data: web::Json<CommentCreate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| CommentError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.comment_service.create(&data, &session.user_id).await
}

#[put("")]
pub async fn update_comment(
    data: web::Json<CommentUpdate>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| CommentError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.comment_service.update(&data, &session.user_id).await
}

#[get("/user/voted")]
pub async fn get_voted(app: web::Data<AppData>, req: HttpRequest) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| CommentError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.comment_service.get_voted(&session.user_id).await
}

#[get("/created/{id}")]
pub async fn get_created(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.comment_service.get_created(&params.id, &user_id).await
}

#[get("/{id}")]
pub async fn get_by_id(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let user_id = match req.cookie(AUTH_COOKIE_NAME) {
        Some(cookie) => app
            .session_service
            .lock()
            .await
            .get_session(cookie.value())
            .await
            .ok()
            .map(|session| session.user_id),
        None => None,
    };

    app.comment_service.find_by_id(&params.id, &user_id).await
}

#[post("/{id}/vote")]
pub async fn vote(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| CommentError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.comment_service.vote(&params.id, &session.user_id).await
}

#[delete("/{id}")]
pub async fn delete(
    params: web::Path<Id>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| CommentError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.comment_service
        .delete(&params.id, &session.user_id)
        .await
}
