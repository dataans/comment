use std::convert::TryFrom;
use std::sync::Arc;

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use thiserror::Error;
use uuid::Uuid;

use crate::model::{Comment, Vote};

const ADD_COMMENT: &str = "insert into comments (id, text, created_at, updated_at, creator_id, sample_id) values ($1, $2, $3, $4, $5, $6);";
const UPDATE_COMMENT: &str = "update comments set text = $2, updated_at = $3 where id = $1";
const FIND_BY_ID: &str = "select * from comments where id = $1";
const DELETE_COMMENT: &str = "delete from comments where id = $1";
const DELETE_COMMENT_UPVOTES: &str = "delete from comments_upvotes where comment_id = $1";
const DELETE_QUESTION_COMMENT: &str = "delete from questions_comments where comment_id = $1";
const DELETE_ANSWER_COMMENT: &str = "delete from answers_comments where comment_id = $1";

const VOTE: &str = "insert into comments_upvotes (comment_id, user_id) values ($1, $2)";
const DELETE_VOTE: &str = "delete from comments_upvotes where comment_id = $1 and user_id = $2";
const FIND_VOTE: &str =
    "select comment_id, user_id from comments_upvotes where comment_id = $1 and user_id = $2";
const COUNT_VOTES_BY_ID: &str = "select count(user_id) from comments_upvotes where comment_id = $1";

const QUERY_CREATED_COMMENTS: &str = "select c.id, c.text, c.created_at, c.updated_at, c.creator_id, c.sample_id from comments as c where c.creator_id = $1";
const QUERY_VOTED_COMMENTS: &str = "select c.id, c.text, c.created_at, c.updated_at, c.creator_id, c.sample_id from comments as c inner join comments_upvotes as cu on cu.comment_id = c.id where cu.user_id = $1";

const ADD_QUESTION_COMMENT: &str =
    "insert into questions_comments (question_id, comment_id) values ($1, $2)";
const ADD_ANSWER_COMMENT: &str =
    "insert into answers_comments (answer_id, comment_id) values ($1, $2)";
const SELECT_QUESTION_COMMENTS: &str = "select c.id, c.text, c.created_at, c.updated_at, c.creator_id, c.sample_id from comments as c inner join questions_comments as qc on qc.comment_id = c.id where question_id = $1";
const SELECT_ANSWER_COMMENTS: &str = "select c.id, c.text, c.created_at, c.updated_at, c.creator_id, c.sample_id from comments as c inner join answers_comments as qa on qa.comment_id = c.id where answer_id = $1";

#[derive(Error, Debug)]
pub enum DbError {
    #[error("Database unavailable")]
    Unavailable,
    #[error("Query error: {0:?}")]
    QueryError(#[from] deadpool_postgres::tokio_postgres::Error),
    #[error("Db connection error: {0:?}")]
    DbConnectionError(#[from] PoolError),
}

pub struct CommentRepository {
    pool: Arc<Mutex<Pool>>,
}

impl CommentRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn save(&self, comment: &Comment) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_COMMENT).await?;

        client
            .query(
                &smt,
                &[
                    &comment.id,
                    &comment.text,
                    &comment.created_at,
                    &comment.updated_at,
                    &comment.creator_id,
                    &comment.sample_id,
                ],
            )
            .await?;

        Ok(())
    }

    pub async fn update(&self, comment: &Comment) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_COMMENT).await?;

        client
            .query(&smt, &[&comment.id, &comment.text, &comment.updated_at])
            .await?;

        Ok(())
    }

    pub async fn add_question_comment(
        &self,
        question_id: &Uuid,
        comment_id: &Uuid,
    ) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_QUESTION_COMMENT).await?;

        client.query(&smt, &[question_id, comment_id]).await?;

        Ok(())
    }

    pub async fn add_answer_comment(
        &self,
        answer_id: &Uuid,
        comment_id: &Uuid,
    ) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(ADD_ANSWER_COMMENT).await?;

        client.query(&smt, &[answer_id, comment_id]).await?;

        Ok(())
    }

    pub async fn find_by_id(&self, comment_id: &Uuid) -> Result<Option<Comment>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[comment_id]).await?.into_iter().next() {
            Some(row) => Ok(Some(Comment::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn select_question_comments(
        &self,
        question_id: &Uuid,
    ) -> Result<Vec<Comment>, DbError> {
        self.query_comments_by_uuid(SELECT_QUESTION_COMMENTS, question_id)
            .await
    }

    async fn query_comments_by_uuid(
        &self,
        query: &str,
        id: &Uuid,
    ) -> Result<Vec<Comment>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(query).await?;

        let rows = client.query(&smt, &[id]).await?;
        let mut comments = Vec::with_capacity(rows.len());
        for row in rows {
            comments.push(Comment::try_from(row)?);
        }

        Ok(comments)
    }

    pub async fn select_answer_comments(&self, answer_id: &Uuid) -> Result<Vec<Comment>, DbError> {
        self.query_comments_by_uuid(SELECT_ANSWER_COMMENTS, answer_id)
            .await
    }

    pub async fn query_created_comments(&self, user_id: &Uuid) -> Result<Vec<Comment>, DbError> {
        self.query_comments_by_uuid(QUERY_CREATED_COMMENTS, user_id)
            .await
    }

    pub async fn query_voted_comments(&self, user_id: &Uuid) -> Result<Vec<Comment>, DbError> {
        self.query_comments_by_uuid(QUERY_VOTED_COMMENTS, user_id)
            .await
    }

    pub async fn count_votes_by_id(&self, comment_id: &Uuid) -> Result<i64, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(COUNT_VOTES_BY_ID).await?;

        match client.query(&smt, &[comment_id]).await?.into_iter().next() {
            Some(row) => Ok(row.try_get::<_, i64>(0)?),
            None => Ok(0),
        }
    }

    pub async fn vote(&self, comment_id: &Uuid, user_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(VOTE).await?;

        client.query(&smt, &[comment_id, user_id]).await?;

        Ok(())
    }

    pub async fn delete_vote(&self, comment_id: &Uuid, user_id: &Uuid) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(DELETE_VOTE).await?;

        client.query(&smt, &[comment_id, user_id]).await?;

        Ok(())
    }

    pub async fn find_vote(
        &self,
        comment_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<Option<Vote>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_VOTE).await?;

        match client
            .query(&smt, &[comment_id, user_id])
            .await?
            .into_iter()
            .next()
        {
            Some(row) => Ok(Some(Vote::try_from(row)?)),
            None => Ok(None),
        }
    }

    async fn simple_execute(&self, id: &Uuid, query: &str) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(query).await?;

        client.query(&smt, &[id]).await?;

        Ok(())
    }

    pub async fn delete_comment(&self, comment_id: &Uuid) -> Result<(), DbError> {
        self.simple_execute(comment_id, DELETE_COMMENT).await
    }

    pub async fn delete_comment_upvotes(&self, comment_id: &Uuid) -> Result<(), DbError> {
        self.simple_execute(comment_id, DELETE_COMMENT_UPVOTES)
            .await
    }

    pub async fn delete_question_comment(&self, comment_id: &Uuid) -> Result<(), DbError> {
        self.simple_execute(comment_id, DELETE_QUESTION_COMMENT)
            .await
    }

    pub async fn delete_answer_comment(&self, comment_id: &Uuid) -> Result<(), DbError> {
        self.simple_execute(comment_id, DELETE_ANSWER_COMMENT).await
    }
}
