use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use async_mutex::Mutex;
use deadpool_postgres::{tokio_postgres::NoTls, Runtime};
use log::error;
use session_manager::SessionService;
use tonic::transport::Server;

use crate::{
    api::handlers::{
        create_comment, default_route, delete, get_by_id, get_created, get_voted, health,
        root_health, update_comment, vote,
    },
    config::{bind_address, check_env_vars, grpc_bind_address, pool_config},
    db::CommentRepository,
    generated::comment::comment_service_server::CommentServiceServer,
    grpc::CommentGrpcServer,
    logging::setup_logger,
    services::comment::CommentService,
};

pub struct AppData {
    pub session_service: Mutex<SessionService>,
    pub comment_service: CommentService,
}

impl AppData {
    pub fn new() -> Self {
        let pool = Arc::new(Mutex::new(
            pool_config()
                .create_pool(Some(Runtime::Tokio1), NoTls)
                .unwrap(),
        ));

        Self {
            session_service: Mutex::new(SessionService::new_from_env()),
            comment_service: CommentService::new(CommentRepository::new(pool)),
        }
    }
}

impl Default for AppData {
    fn default() -> Self {
        Self::new()
    }
}

#[allow(deprecated)]
pub async fn start_app() -> Vec<tokio::task::JoinHandle<Result<(), ()>>> {
    check_env_vars();
    setup_logger();

    let http_server = HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .default_service(actix_web::web::route().to(default_route))
            .data(AppData::new())
            .service(root_health)
            .service(
                web::scope("/api/v1/comment")
                    .service(health)
                    .service(create_comment)
                    .service(update_comment)
                    .service(get_by_id)
                    .service(vote)
                    .service(delete)
                    .service(get_created)
                    .service(get_voted),
            )
    })
    .bind(bind_address())
    .unwrap()
    .workers(4)
    .run();

    let http_server = tokio::spawn(async move {
        match http_server.await {
            Ok(_) => {}
            Err(err) => error!("http server error: {:?}", err),
        };

        Ok(())
    });

    let grpc_server = tokio::spawn(async move {
        match Server::builder()
            .add_service(CommentServiceServer::new(CommentGrpcServer::new(
                CommentService::new(CommentRepository::new(Arc::new(Mutex::new(
                    pool_config()
                        .create_pool(Some(Runtime::Tokio1), NoTls)
                        .unwrap(),
                )))),
            )))
            .serve(grpc_bind_address().parse().unwrap())
            .await
        {
            Ok(_) => {}
            Err(err) => error!("grpc server error: {:?}", err),
        };

        Ok(())
    });

    vec![http_server, grpc_server]
}
