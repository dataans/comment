use std::convert::TryFrom;

use deadpool_postgres::tokio_postgres::Row;
use time::OffsetDateTime;
use uuid::Uuid;

pub enum CommentType {
    Question(Uuid),
    Answer(Uuid),
}

pub struct Comment {
    pub id: Uuid,
    pub text: String,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub creator_id: Uuid,
    pub sample_id: Option<Uuid>,
}

impl TryFrom<Row> for Comment {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            text: row.try_get(1)?,
            created_at: row.try_get(2)?,
            updated_at: row.try_get(3)?,
            creator_id: row.try_get(4)?,
            sample_id: row.try_get(5)?,
        })
    }
}

pub struct CommentToUser {
    #[allow(dead_code)]
    comment_id: Uuid,
    #[allow(dead_code)]
    user_id: Uuid,
}

impl TryFrom<Row> for CommentToUser {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            comment_id: row.try_get(0)?,
            user_id: row.try_get(1)?,
        })
    }
}

pub type Vote = CommentToUser;
