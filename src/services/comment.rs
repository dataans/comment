use actix_web::{http::StatusCode, ResponseError};
use session_manager::SessionError;
use thiserror::Error;
use time::OffsetDateTime;
use uuid::Uuid;

use crate::{
    api::{
        types::{
            CommentCreate, CommentResponse, CommentTypeRequest, CommentUpdate, CommentsResponse,
        },
        EmptyResponse,
    },
    db::{CommentRepository, DbError},
    model::Comment,
};

#[derive(Error, Debug)]
pub enum CommentError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("Comment with id {0:?} not found")]
    NotFound(Uuid),
    #[error("Permission denied: {0}")]
    PermissionDenied(String),
}

impl From<SessionError> for CommentError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => CommentError::NotAuthorized(error),
            SessionError::Crypto(error) => CommentError::Internal(error),
            SessionError::RedisConnection(error) => CommentError::Internal(format!("{}", error)),
        }
    }
}

impl ResponseError for CommentError {
    fn status_code(&self) -> StatusCode {
        match self {
            CommentError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            CommentError::NotFound(_) => StatusCode::NOT_FOUND,
            CommentError::PermissionDenied(_) => StatusCode::FORBIDDEN,
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct CommentService {
    comment_repository: CommentRepository,
}

impl CommentService {
    pub fn new(comment_repository: CommentRepository) -> Self {
        Self { comment_repository }
    }

    pub async fn create(
        &self,
        data: &CommentCreate,
        user_id: &Uuid,
    ) -> Result<CommentResponse, CommentError> {
        let CommentCreate {
            text,
            sample_id,
            comment_type,
        } = data.clone();

        let comment = Comment {
            id: Uuid::new_v4(),
            text,
            created_at: OffsetDateTime::now_utc(),
            updated_at: OffsetDateTime::now_utc(),
            creator_id: *user_id,
            sample_id,
        };

        self.comment_repository.save(&comment).await?;

        match comment_type {
            CommentTypeRequest::Question(question_id) => {
                self.comment_repository
                    .add_question_comment(&question_id, &comment.id)
                    .await?;
            }
            CommentTypeRequest::Answer(answer_id) => {
                self.comment_repository
                    .add_answer_comment(&answer_id, &comment.id)
                    .await?
            }
        };

        Ok(comment.into())
    }

    pub async fn update(
        &self,
        data: &CommentUpdate,
        user_id: &Uuid,
    ) -> Result<CommentResponse, CommentError> {
        let CommentUpdate { id, text } = data.clone();

        let mut comment = self
            .comment_repository
            .find_by_id(&id)
            .await?
            .ok_or_else(|| CommentError::NotFound(data.id))?;

        if comment.creator_id != *user_id {
            return Err(CommentError::PermissionDenied(
                "User can edit only their created comments".into(),
            ));
        }

        comment.text = text;
        comment.updated_at = OffsetDateTime::now_utc();

        self.comment_repository.update(&comment).await?;

        Ok(comment.into())
    }

    pub async fn find_by_id(
        &self,
        comment_id: &Uuid,
        user_id: &Option<Uuid>,
    ) -> Result<CommentResponse, CommentError> {
        let mut comment: CommentResponse =
            match self.comment_repository.find_by_id(comment_id).await? {
                Some(comment) => comment,
                None => return Err(CommentError::NotFound(*comment_id)),
            }
            .into();

        comment.set_up_votes(
            self.comment_repository
                .count_votes_by_id(comment.id())
                .await?,
        );

        if let Some(user_id) = user_id {
            comment.set_is_voted(
                self.comment_repository
                    .find_vote(comment.id(), user_id)
                    .await?
                    .is_some(),
            );
        }

        Ok(comment)
    }

    async fn set_votes_params(
        &self,
        comments: Vec<Comment>,
        user_id: &Option<Uuid>,
    ) -> Result<Vec<CommentResponse>, CommentError> {
        let mut votes = Vec::with_capacity(comments.len());
        for comment in comments.iter() {
            votes.push(
                self.comment_repository
                    .count_votes_by_id(&comment.id)
                    .await?,
            );
        }

        let is_voted = match user_id {
            Some(user_id) => {
                let mut is_voted = Vec::with_capacity(comments.len());
                for comment in comments.iter() {
                    is_voted.push(
                        self.comment_repository
                            .find_vote(&comment.id, user_id)
                            .await?
                            .is_some(),
                    );
                }
                is_voted
            }
            None => vec![false; comments.len()],
        };

        Ok(comments
            .into_iter()
            .zip(votes.into_iter())
            .zip(is_voted.into_iter())
            .map(|((comment, up_votes), is_voted)| {
                let Comment {
                    id,
                    text,
                    created_at,
                    updated_at,
                    creator_id,
                    sample_id,
                } = comment;
                CommentResponse {
                    id,
                    text,
                    created_at,
                    updated_at,
                    creator_id,
                    sample_id,
                    up_votes,
                    is_voted,
                }
            })
            .collect())
    }

    pub async fn get_question_comments(
        &self,
        question_id: &Uuid,
        user_id: &Option<Uuid>,
    ) -> Result<Vec<CommentResponse>, CommentError> {
        let comments = self
            .comment_repository
            .select_question_comments(question_id)
            .await?;

        self.set_votes_params(comments, user_id).await
    }

    pub async fn get_created(
        &self,
        user_id: &Uuid,
        current_user_id: &Option<Uuid>,
    ) -> Result<CommentsResponse, CommentError> {
        let comments = self
            .comment_repository
            .query_created_comments(user_id)
            .await?;

        Ok(CommentsResponse(
            self.set_votes_params(comments, current_user_id).await?,
        ))
    }

    pub async fn get_voted(&self, user_id: &Uuid) -> Result<CommentsResponse, CommentError> {
        let comments = self
            .comment_repository
            .query_voted_comments(user_id)
            .await?;

        Ok(CommentsResponse(
            self.set_votes_params(comments, &Some(*user_id)).await?,
        ))
    }

    pub async fn get_answer_comments(
        &self,
        question_id: &Uuid,
        user_id: &Option<Uuid>,
    ) -> Result<Vec<CommentResponse>, CommentError> {
        let comments = self
            .comment_repository
            .select_answer_comments(question_id)
            .await?;

        self.set_votes_params(comments, user_id).await
    }

    pub async fn vote(
        &self,
        comment_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<CommentResponse, CommentError> {
        if self
            .comment_repository
            .find_by_id(comment_id)
            .await?
            .is_none()
        {
            return Err(CommentError::NotFound(*comment_id));
        }

        match self
            .comment_repository
            .find_vote(comment_id, user_id)
            .await?
        {
            Some(_) => {
                self.comment_repository
                    .delete_vote(comment_id, user_id)
                    .await?
            }
            None => self.comment_repository.vote(comment_id, user_id).await?,
        };

        self.find_by_id(comment_id, &Some(*user_id)).await
    }

    pub async fn delete(
        &self,
        comment_id: &Uuid,
        user_id: &Uuid,
    ) -> Result<EmptyResponse, CommentError> {
        let comment = self
            .comment_repository
            .find_by_id(comment_id)
            .await?
            .ok_or_else(|| CommentError::NotFound(*comment_id))?;

        if comment.creator_id != *user_id {
            return Err(CommentError::PermissionDenied(
                "User can delete only their created comments".into(),
            ));
        }

        self.comment_repository
            .delete_comment_upvotes(comment_id)
            .await?;
        self.comment_repository
            .delete_answer_comment(comment_id)
            .await?;
        self.comment_repository
            .delete_question_comment(comment_id)
            .await?;
        self.comment_repository.delete_comment(comment_id).await?;

        Ok(EmptyResponse::new(StatusCode::NO_CONTENT))
    }
}
