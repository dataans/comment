use std::sync::Arc;

use prost_types::Timestamp;
use thiserror::Error;
use tonic::{Request, Response, Status};
use uuid::Uuid;

use crate::{
    api::types::{CommentCreate, CommentResponse, CommentTypeRequest, CommentUpdate},
    generated::comment::{
        comment_service_server::CommentService as GrpcCommentService, Comment,
        CommentCreate as GrpcCommentCreate, CommentUpdate as GrpcCommentUpdate, Comments,
        CommentsOptions, Parent,
    },
    services::comment::{CommentError, CommentService},
};

#[derive(Error, Debug)]
pub enum GrpcError {
    #[error("uuid parse error: {0:?}")]
    Uuid(#[from] uuid::Error),
    #[error("unsupported: {0}")]
    Unsupported(String),
    #[error("Missing: {0}")]
    Missing(String),
}

impl From<GrpcError> for Status {
    fn from(error: GrpcError) -> Self {
        Status::invalid_argument(error.to_string())
    }
}

impl From<CommentError> for Status {
    fn from(error: CommentError) -> Self {
        match error {
            CommentError::NotAuthorized(err) => Status::unauthenticated(err),
            err => Status::internal(err.to_string()),
        }
    }
}

impl From<CommentResponse> for Comment {
    fn from(comment: CommentResponse) -> Self {
        let CommentResponse {
            id,
            text,
            created_at,
            updated_at,
            creator_id,
            sample_id,
            up_votes,
            is_voted,
        } = comment;
        Self {
            id: id.to_string(),
            text,
            creator_id: creator_id.to_string(),
            sample_id: sample_id.map(|s| s.to_string()),
            created_at: Some(Timestamp {
                seconds: created_at.unix_timestamp(),
                nanos: 0,
            }),
            updated_at: Some(Timestamp {
                seconds: updated_at.unix_timestamp(),
                nanos: 0,
            }),
            up_votes,
            is_voted,
        }
    }
}

impl TryFrom<Parent> for CommentTypeRequest {
    type Error = GrpcError;

    fn try_from(parent: Parent) -> Result<Self, GrpcError> {
        let Parent {
            comment_type,
            parent_id,
        } = parent;
        let id = Uuid::parse_str(parent_id.as_str())?;

        match comment_type {
            0 => Ok(CommentTypeRequest::Question(id)),
            1 => Ok(CommentTypeRequest::Answer(id)),
            n => Err(GrpcError::Unsupported(format!("comment type: {}", n))),
        }
    }
}

impl TryFrom<GrpcCommentCreate> for CommentCreate {
    type Error = GrpcError;

    fn try_from(comment: GrpcCommentCreate) -> Result<Self, GrpcError> {
        let GrpcCommentCreate {
            sample_id,
            text,
            parent,
            ..
        } = comment;
        let sample_id = match sample_id {
            Some(id) => Some(Uuid::parse_str(id.as_str())?),
            None => None,
        };

        Ok(Self {
            text,
            sample_id,
            comment_type: CommentTypeRequest::try_from(
                parent.ok_or_else(|| GrpcError::Missing("comment_type".into()))?,
            )?,
        })
    }
}

pub struct CommentGrpcServer {
    comment_service: Arc<CommentService>,
}

impl CommentGrpcServer {
    pub fn new(comment_service: CommentService) -> Self {
        Self {
            comment_service: Arc::new(comment_service),
        }
    }
}

#[tonic::async_trait]
impl GrpcCommentService for CommentGrpcServer {
    async fn create_comment(
        &self,
        request: Request<GrpcCommentCreate>,
    ) -> Result<Response<Comment>, Status> {
        let comment_create = request.into_inner();

        let user_id = Uuid::parse_str(comment_create.creator_id.as_str())
            .map_err(|err| Status::from(GrpcError::Uuid(err)))?;
        let comment_create = CommentCreate::try_from(comment_create)?;

        let comment = self
            .comment_service
            .create(&comment_create, &user_id)
            .await?;

        Ok(Response::new(comment.into()))
    }

    async fn update_comment(
        &self,
        request: Request<GrpcCommentUpdate>,
    ) -> Result<Response<Comment>, Status> {
        let GrpcCommentUpdate {
            comment_id,
            text,
            creator_id,
        } = request.into_inner();

        let id = Uuid::parse_str(comment_id.as_str())
            .map_err(|err| Status::from(GrpcError::Uuid(err)))?;

        let user_id = Uuid::parse_str(creator_id.as_str())
            .map_err(|err| Status::from(GrpcError::Uuid(err)))?;

        let comment = self
            .comment_service
            .update(&CommentUpdate { id, text }, &user_id)
            .await?;

        Ok(Response::new(comment.into()))
    }

    async fn get_comments(
        &self,
        request: Request<CommentsOptions>,
    ) -> Result<Response<Comments>, Status> {
        let CommentsOptions { parent, user_id } = request.into_inner();

        let Parent {
            comment_type,
            parent_id,
        } = parent.ok_or_else(|| Status::invalid_argument("Missing parent parameter"))?;
        let user_id = match user_id {
            Some(user_id) => Some(
                Uuid::parse_str(user_id.as_str())
                    .map_err(|err| Status::from(GrpcError::Uuid(err)))?,
            ),
            None => None,
        };

        Ok(Response::new(Comments {
            comments: match comment_type {
                0 => {
                    self.comment_service
                        .get_question_comments(
                            &Uuid::parse_str(parent_id.as_str())
                                .map_err(|err| Status::from(GrpcError::Uuid(err)))?,
                            &user_id,
                        )
                        .await?
                }
                1 => {
                    self.comment_service
                        .get_answer_comments(
                            &Uuid::parse_str(parent_id.as_str())
                                .map_err(|err| Status::from(GrpcError::Uuid(err)))?,
                            &user_id,
                        )
                        .await?
                }
                n => return Err(GrpcError::Unsupported(format!("comment type: {}", n)).into()),
            }
            .into_iter()
            .map(|c| c.into())
            .collect(),
        }))
    }
}
