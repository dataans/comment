FROM rust:latest

RUN USER=root cargo new --bin comment

WORKDIR ./comment

COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
